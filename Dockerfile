FROM python:3.9-slim-buster

ENV PYTHONUNBUFFERED 1

COPY python-django-assessment/requirements.txt /requirements.txt
RUN pip3 install wheel \
                 pyzmq \
                 numpy \
                 kiwisolver
RUN pip install -r /requirements.txt && \
    mkdir /python-django-assessment

EXPOSE 8000
	
WORKDIR /python-django-assessment
COPY ./python-django-assessment /python-django-assessment
