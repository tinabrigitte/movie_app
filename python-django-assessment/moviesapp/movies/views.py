# -*- coding: utf-8 -*-

"""Movies views."""

from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib import messages
from django.shortcuts import redirect
from django.http import Http404
from django.urls import reverse_lazy, reverse

from .models import Movie
from .forms import MovieForm


class MovieListView(ListView):
    """Show all movies."""

    model = Movie
    template_name = 'movies/movie_list.html'

    def get_queryset(self):

        queryset = super(MovieListView, self).get_queryset()
        queryset = queryset.order_by('-released_on', '-rated')
        return queryset

    def get_success_url(self):
        return reverse('index')


class MovieDetailView(DetailView):
    """Show the requested movie."""
    model = Movie
    template_name = 'movies/movie_detail.html'

    def get_object(self):
        return Movie.objects.get(pk=self.kwargs['id'])

    def get_success_url(self):
        return reverse('movie:index')


class MovieCreateView(CreateView):
    """Create a new movie."""
    model = Movie
    template_name = 'movies/movie_form.html'
    form_class = MovieForm

    def form_valid(self, form):
        form.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse('movie:index')


class MovieUpdateView(UpdateView):
    """Update the requested movie."""
    model = Movie
    template_name = 'movies/movie_form.html'
    form_class = MovieForm

    def get_object(self):
        return Movie.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        form.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse('movie:index')


class MovieDeleteView(DeleteView):
    """Delete the requested movie."""
    model = Movie
    template_name = 'movies/movie_confirm_delete.html'

    def get_success_url(self):
        return reverse('movie:index')
